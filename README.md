# BASE DOCKER WORDPRESS

## Antes de iniciar

Copiar .env.example a .env y cambiar los valores a conveniencia.

El valor de PROJECT que no se repita con otros proyectos en la misma máquina.

## Inicialización

El proyecto genera una carpeta ./code la primera vez que se levanta el proyetco mediante:
```sh
docker-compose up -d
```

En subsecuentes inicios monta ./code en la carpeta html.

